from contextlib import ExitStack
from random import randint
name = input("Hi! What is your name? ")
num = (1)
num_of_guesses = 5
for birthday_guess in range(num_of_guesses):
    month = randint(1, 12)
    year = randint(1924, 2004)
    print("Guess",num,": ", name, "were you born in", month, year, "?")
    print("yes or no?")
    answer = input("Answer:")
    num = num +1
    if answer == "yes" or answer == "Yes":
        print("I knew it!")
        exit()
    elif  num > (5):
        print("I have other things to do. Good bye.")
        exit()
    elif answer == "no" or answer == "No":
        print("Drat! Lemme try again!")
